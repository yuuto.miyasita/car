﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; //Text
using System; // File
using UnityEngine.Networking;



/// <summary>
/// Manager main.
/// </summary>
public class ManagerMain : MonoBehaviour
{
    [SerializeField] private Text _displayField = default;
    [SerializeField] private Text inputFieldName = default;
    [SerializeField] private Text ScareText = default;

    private List<MemberData> _memberList;
   // private string ScareText;


    /// <summary>
    /// Raises the click clear display event.
    /// </summary>
    public void OnClickClearDisplay()
    {
        _displayField.text = " ";
    }

    /// <summary>
    /// Raises the click get json from www event.
    /// </summary>
    public void OnClickGetJsonFromWebRequest()　　　　　　　　　　　　　//押されたらここから
    {
        _displayField.text = "wait...";
        GetJsonFromWebRequest();
       
    }

    /// <summary>
    /// Raises the click show member list
    /// </summary>
    public void OnClickShowMemberList()
    {
        string sStrOutput = "";

        if (null == _memberList)
        {
            sStrOutput = "no list !";
        }
        else
        {
            //リストの内容を表示
            foreach (MemberData memberOne in _memberList)
            {
                sStrOutput += $"name:{memberOne.Name}  message:{memberOne.Score} \n";
            }
        }

        _displayField.text = sStrOutput;
    }


    /// <summary>
    /// Gets the json from www.
    /// </summary>
    private void GetJsonFromWebRequest()
    {
        // API を呼んだ際に想定されるレスポンス
        // [{"name":"\u3072\u3068\u308a\u3081","age":123,"hobby":"\u30b4\u30eb\u30d5"},{"name":"\u3075\u305f\u308a\u3081","age":25,"hobby":"walk"},{"name":"\u3055\u3093\u306b\u3093\u3081","age":77,"hobby":"\u5c71"}]
        //
        //APIが設置してあるパス
        string sTgtURL = "http://localhost/saisyuukadai/saisyuukadai/getRanking";

        // Wwwを利用して json データ取得をリクエストする
        StartCoroutine(
            DownloadJson(sTgtURL,CallbackWebRequestSuccess, CallbackWebRequestFailed)// APIコールが成功した際に呼ばれる関数を指定
                                                                                 // APIコールが失敗した際に呼ばれる関数を指定

        );
        
    }

    /// <summary>
    /// Callbacks the www success.
    /// </summary>
    /// <param name="response">Response.</param>
    private void CallbackWebRequestSuccess(string response)
    {
        //Json の内容を MemberData型のリストとしてデコードする。
        _memberList = MemberDataModel.DeserializeFromJson(response);

        //memberList ここにデコードされたメンバーリストが格納される。

        _displayField.text = "Success!";
    }

    /// <summary>
    /// Callbacks the www failed.
    /// </summary>
    private void CallbackWebRequestFailed()
    {
        // jsonデータ取得に失敗した
        _displayField.text = "WebRequest Failed";
    }

    /// <summary>
    /// Downloads the json.
    /// </summary>
    /// <returns>The json.</returns>
    /// <param name="cbkSuccess">Cbk success.</param>
    /// <param name="cbkFailed">Cbk failed.</param>
    private IEnumerator DownloadJson(string url,Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        var www= UnityWebRequest.Get(url);
        yield return www.SendWebRequest();
        if (www.error != null)
        {
            //レスポンスエラーの場合
            Debug.LogError(www.error);
            if (null != cbkFailed)
            {
                cbkFailed();
            }
        }
        else if (www.isDone)
        {
            // リクエスト成功の場合
            Debug.Log($"Success:{www.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(www.downloadHandler.text);
            }
        }
    }
    public void OnclicSetMessage()
    {
        _displayField.text = "wait....";
        SetJsonFromWWW();
    }
    private void SetJsonFromWWW()
    {
        string sTgtURL  = "http://localhost/saisyuukadai/saisyuukadai/setRanking";
        //セットする情報
        string name = inputFieldName.text;
        //string Scare = ScareText.text;​
        StartCoroutine(SetMessage(sTgtURL, name, ScareText.text, CallbackWebRequestSuccess, CallbackWebRequestFailed));
    }
    private IEnumerator SetMessage(string url, string name, string Scare, Action<string> cbkSuccess = null, Action cbkFailed = null)
    {
        WWWForm form = new WWWForm();
        form.AddField("name", name);//右のnameは返す
        form.AddField("Scare", this.ScareText.text);
        UnityWebRequest webRequest = UnityWebRequest.Post(url, form);
        webRequest.timeout = 5;//つながらないとき5秒待つ​
        yield return webRequest.SendWebRequest();
        if (webRequest.error != null)
        {
            Debug.LogError(webRequest.error);
            if (null != cbkFailed)//cbkを呼ばれなかったらcbkを呼ぶ
            {
                cbkFailed();
            }
        }
        else if (webRequest.isDone)
        {
            Debug.Log($"Success:{webRequest.downloadHandler.text}");
            if (null != cbkSuccess)
            {
                cbkSuccess(webRequest.downloadHandler.text);
            }
        }
    }
    
}
