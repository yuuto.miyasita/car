﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clear : MonoBehaviour
{
    public GameObject clear;
    // Start is called before the first frame update
    void Start()
    {
        clear.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("OnTrriger1");
        if (collider.gameObject.tag == "Player")
        {
            Debug.Log("OnTrriger");
            clear.SetActive(true);
        }
    }
    // 衝突している間呼ばれ続ける  
    void OnTriggerStay(Collider other)
    {
        Debug.Log("OnTrriger2");
    }
    // 衝突から離れた瞬間に呼ばれる  
    void OnTriggerExit(Collider other)
    {
        Debug.Log("OnTrriger3");
    }


    // 衝突した瞬間に呼ばれる  
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("OnColider1");
    }
    // 衝突している間呼ばれ続ける  
    void OnCollisionStay(Collision collision)
    {
        Debug.Log("OnColider2");
    }
    // 衝突から離れた瞬間に呼ばれる  
    void OnCollisionExit(Collision collision)
    {
        Debug.Log("OnColider3");
    }
}

