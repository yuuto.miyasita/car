﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bar : MonoBehaviour
{
    bool isGameStart = false;
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Count", 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        if (!isGameStart) return;
        if (isGameStart)
        {
            Count();
        }
    }
    void Count()
    {
        Destroy(this.gameObject);
    }
}
