﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeScript : MonoBehaviour
{

    [SerializeField]
    private int minute;

    public float seconds;
    //　前のUpdateの時の秒数
    private float oldSeconds;
    //　タイマー表示用テキスト
    private Text timerText;
    bool isGameStart = false;

    void Start()
    {
        Invoke("Count", 2.0f);
        //3.5秒後に実行する
        minute = 0;
        seconds = 0f;
        oldSeconds = 0f;
        timerText = GetComponentInChildren<Text>();

    }
    void Update()
    {
        if (!isGameStart) return;
        if (isGameStart)
        {
            Count();
        }
    }
    void Count()
    {
        isGameStart = true;
        seconds += Time.deltaTime;
        if (seconds >= 60f)
        {
            minute++;
            seconds = seconds - 60;
        }
        //　値が変わった時だけテキストUIを更新
        if ((int)seconds != (int)oldSeconds)
        {
            timerText.text = minute.ToString("00") + ":" + ((int)seconds).ToString("00");
        }
        oldSeconds = seconds;
    }
}