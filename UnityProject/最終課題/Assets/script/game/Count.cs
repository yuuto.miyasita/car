﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Count : MonoBehaviour
{
    //public float MAX_TIME = 5.0f; // カウントダウンの開始値
    //float MAX_TIME = MAX_TIME;
    [SerializeField] private float MAX_TIME;
    
    void Start()
    {
        GetComponent<UnityEngine.UI.Text>().text = MAX_TIME.ToString();
    }

    void Update()
    {
        MAX_TIME -= Time.deltaTime;

        // マイナス値にならないようにしている
        MAX_TIME = Mathf.Max(MAX_TIME, 1.0f);
        GetComponent<UnityEngine.UI.Text>().text = ((int)MAX_TIME).ToString();
        if (MAX_TIME == 1.0f)
        {
            GetComponent<Text>().text = "スタート！！";
            
        }
    }
}
