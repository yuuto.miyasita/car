﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartC : MonoBehaviour
{
    [SerializeField]
    private int minute;
    [SerializeField]
    private float seconds;
    //　前のUpdateの時の秒数
    private float oldSeconds;
    //　タイマー表示用テキスト
    private Text timerText;

    // Start is called before the first frame update
    void Start()
    {
        //DelayMethodを3.5秒後に呼び出す
        Invoke("DelayMethod", 3.5f);
    }

    // Update is called once per frame
    void DelayMethod()
    {
        
    }
}
