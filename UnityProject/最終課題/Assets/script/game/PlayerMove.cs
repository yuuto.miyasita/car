﻿using UnityEngine;
using UnityEngine.AI; //NavMshAgentコンポーネントを使用するために必要なusing

namespace Sample.Script
{
    /// <summary>
    /// プレイヤーの移動制御
    /// </summary>
    public class PlayerMove : MonoBehaviour
    {
        [SerializeField] private Transform destinationTransform; //目的地のTransformを指定

        private void Start()
        {
            NavMeshAgent navMeshAgent = GetComponent<NavMeshAgent>(); //ここでNavMeshAgentをGetComponentする

            navMeshAgent.SetDestination(destinationTransform.position); //NavMeshAgentの目的地を設定
        }
    }
}
