﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameContoroller : MonoBehaviour
{
    public Button Button;

    // Use this for initialization
    void Start()
    {
        //Button = GetComponent<GameObject>();
        Button.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider collider)
    { 
        if (collider.gameObject.CompareTag("Player"))
        {
            Button.gameObject.SetActive(true);
        }
    }
}
