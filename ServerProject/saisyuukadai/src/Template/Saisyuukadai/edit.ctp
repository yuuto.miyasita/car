<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Saisyuukadai $saisyuukadai
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $saisyuukadai->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $saisyuukadai->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Saisyuukadai'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="saisyuukadai form large-9 medium-8 columns content">
    <?= $this->Form->create($saisyuukadai) ?>
    <fieldset>
        <legend><?= __('Edit Saisyuukadai') ?></legend>
        <?php
            echo $this->Form->control('Name');
            echo $this->Form->control('Scare');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
