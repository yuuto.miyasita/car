<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Saisyuukadai[]|\Cake\Collection\CollectionInterface $saisyuukadai
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Saisyuukadai'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="saisyuukadai index large-9 medium-8 columns content">
    <h3><?= __('Saisyuukadai') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Scare') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($saisyuukadai as $saisyuukadai): ?>
            <tr>
                <td><?= $this->Number->format($saisyuukadai->id) ?></td>
                <td><?= h($saisyuukadai->Name) ?></td>
                <td><?= $this->Number->format($saisyuukadai->Scare) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $saisyuukadai->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $saisyuukadai->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $saisyuukadai->id], ['confirm' => __('Are you sure you want to delete # {0}?', $saisyuukadai->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
