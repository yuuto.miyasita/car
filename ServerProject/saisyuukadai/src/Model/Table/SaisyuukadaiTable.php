<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Saisyuukadai Model
 *
 * @method \App\Model\Entity\Saisyuukadai get($primaryKey, $options = [])
 * @method \App\Model\Entity\Saisyuukadai newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Saisyuukadai[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Saisyuukadai|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Saisyuukadai saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Saisyuukadai patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Saisyuukadai[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Saisyuukadai findOrCreate($search, callable $callback = null, $options = [])
 */
class SaisyuukadaiTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('saisyuukadai');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('Name')
            ->maxLength('Name', 30)
            ->requirePresence('Name', 'create')
            ->notEmptyString('Name');

        $validator
            ->integer('Scare')
            ->requirePresence('Scare', 'create')
            ->notEmptyString('Scare');

        return $validator;
    }
}
