<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SaisyuukadaiTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SaisyuukadaiTable Test Case
 */
class SaisyuukadaiTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SaisyuukadaiTable
     */
    public $Saisyuukadai;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Saisyuukadai'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Saisyuukadai') ? [] : ['className' => SaisyuukadaiTable::class];
        $this->Saisyuukadai = TableRegistry::getTableLocator()->get('Saisyuukadai', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Saisyuukadai);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
